use ansi_term::Color;
use notify::{Watcher, RecursiveMode, watcher, DebouncedEvent};
use std::collections::HashMap;
use std::hash::Hasher;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use std::process::{Child, Command};
use std::sync::mpsc::channel;
use std::time::Duration;
use twox_hash::XxHash;

enum LogType {
    Error,
    Warn,
    Info
}

pub struct FileWatcher {
    watch_dir: String,
    hashes: HashMap<String, u64>,
    cmd: String,
    args: Vec<String>,
    child: Option<Child>
}

impl FileWatcher {
    // create new file watcher
    pub fn new(dir: &str, files: &Vec<&str>, cmd: &str, args: &Vec<&str>) -> Result<FileWatcher,()> {
        let mut hashes: HashMap<String, u64> = HashMap::new();

        let watch_dir;

        // normalize path of watch dir
        match fs::canonicalize(dir) {
            Err(reason) => {
                log_error(format!("watch dir does not exist: {}", reason));
                return Err(());
            },
            Ok(dir_buff) => watch_dir = path_buf_to_str(&dir_buff)
        }

        // create initial file hashes
        for file in files {
            let cur_path: String;

            match fs::canonicalize(file) {
                Ok(file_pth) => cur_path = path_buf_to_str(&file_pth),
                Err(_) => {
                    log_warn(format!("ignoring non-existing file: {}", file));
                    continue;
                }
            }

            if !cur_path.starts_with(&watch_dir) {
                log_warn(format!("ignoring file outside of watch dir: {}", file));
                continue;
            }

            hashes.insert(cur_path.clone(), get_file_hash(&cur_path));
        }

        if hashes.is_empty() {
            log_error(format!("no files to watch"));
            return Err(());
        }

        return Ok(FileWatcher {
            watch_dir,
            hashes,
            cmd: cmd.to_owned(),
            args: args.into_iter().map(|s| s.to_string()).collect(),
            child: None
        });
    }

    // start watching
    pub fn start(mut self) {
        log_info(format!("watching dir: {}", self.watch_dir));

        log_info(format!("for files:"));
        for (key,_) in &self.hashes {
            log_info(format!("\t{}", key));
        }

        let (tx, rx) = channel();
        let mut watcher = watcher(tx, Duration::from_secs(2)).unwrap();

        self.start_command();

        watcher.watch(self.watch_dir.clone(), RecursiveMode::Recursive).unwrap();

        loop {
            match rx.recv() {
                Ok(event) => {
                    self.handle_event(event)
                },
                Err(e) => log_error(format!("watch error: {:?}", e)),
            }
        }
    }

    // start thread for command
    fn start_command(&mut self) {
        self.kill_child();

        log_info(format!("(re)starting command..."));

        self.child = Command::new(&self.cmd)
            .args(&self.args)
            .spawn()
            .ok();

        if self.child.is_none() {
            log_error(format!("could not start command."));
        }
    }

    // kill thread of watch command
    fn kill_child(&mut self) {
        if let Some(child) = self.child.as_mut() {
            child.kill().ok();
            child.wait().ok();
        }
        self.child = None;
    }

    // handle watch event
    fn handle_event(&mut self, evt: DebouncedEvent) {
        let mut restart = false;

        let pth_str = get_event_path(evt);

        if let Some(hash) = self.hashes.get_mut(&pth_str) {
            let new_hash = get_file_hash(&pth_str);

            if *hash != new_hash {
                *hash = new_hash;
                restart = true;
            }
        }

        if restart {
            self.start_command();
        }
    }
}

// extract relevant path from watch event
fn get_event_path(evt: DebouncedEvent) -> String {
    match evt {
        DebouncedEvent::NoticeWrite(pth) |
        DebouncedEvent::Create(pth) |
        DebouncedEvent::Write(pth) |
        DebouncedEvent::Chmod(pth) => path_buf_to_str(&pth),
        DebouncedEvent::Rename(_, pth) => path_buf_to_str(&pth),
        _ => String::new()
    }
}

// convert path buffer into string
fn path_buf_to_str(pth: &PathBuf) -> String {
    pth.to_string_lossy().into_owned()
}

// get hash for given file
fn get_file_hash(pth: &str) -> u64 {
    let mut hasher = XxHash::default();

    let mut file;

    match File::open(pth) {
        Err(_) => return hasher.finish(),
        Ok(fhandle) => file = fhandle
    }

    let mut buffer = [0; 1024 * 512];

    while let Ok(bytes_read) = file.read(&mut buffer) {
        if bytes_read == 0 {
            break;
        }
        hasher.write(&buffer[..bytes_read]);
    }

    hasher.finish()
}

fn log(log_type: LogType, msg: &str) {
    let type_str;

    match log_type {
        LogType::Error => type_str = Color::Red.paint("ERROR"),
        LogType::Warn => type_str = Color::Yellow.paint("WARN"),
        LogType::Info => type_str = Color::Blue.paint("INFO")
    }

    println!("[{}][{}] {}", Color::Blue.paint("hashmon"), type_str, msg);
}

fn log_info(msg: String) {
    log(LogType::Info, &msg);
}

fn log_warn(msg: String) {
    log(LogType::Warn, &msg);
}

fn log_error(msg: String) {
    log(LogType::Error, &msg);
}
