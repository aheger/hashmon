extern crate ansi_term;
extern crate clap;
extern crate getopts;
extern crate notify;
extern crate twox_hash;

mod file_watcher;

use clap::{Arg, App};
use file_watcher::FileWatcher;

fn main() {
    // parse cli arguments
    let matches = App::new("hashmon")
        .version("0.2")
        .author("Andreas Heger <aheger82@gmail.com>")
        .about("Watch files for change of their hash")
        .arg(Arg::with_name("command")
             .short("c")
             .long("command")
             .value_name("COMMAND")
             .takes_value(true)
             .required(true)
             .help("Command that is executed when a change was detected")
        )
        .arg(Arg::with_name("arg")
             .short("a")
             .long("arg")
             .value_name("ARG")
             .takes_value(true)
             .multiple(true)
             .help("Additional arguments for the command that is executed when a change was detected")
        )
        .arg(Arg::with_name("dir")
             .short("d")
             .long("dir")
             .value_name("DIR")
             .takes_value(true)
             .help("The directory that is watched for changes")
        )
        .arg(Arg::with_name("file")
             .short("f")
             .long("file")
             .value_name("FILE")
             .takes_value(true)
             .multiple(true)
             .required(true)
             .help("File(s) inside the watched directory (or its descendants) that are watched for hash change")
        )
        .get_matches();

    //create watcher and start
    let watcher_new = FileWatcher::new(
        matches.value_of("dir").unwrap_or("."),
        &matches.values_of("file").unwrap().collect::<Vec<&str>>(),
        matches.value_of("command").unwrap(),
        &matches.values_of("arg").unwrap_or_default().collect::<Vec<&str>>()
    );

    match watcher_new {
        Ok(watcher) => watcher.start(),
        Err(_) => std::process::exit(1)
    }
}
