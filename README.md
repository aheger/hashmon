# hashmon
Watch files for change of their hash
## Basic usage
```
hashmon --dir=/watch/dir --file=/watch/dir/file --command=ls --arg=-l

--dir       the directory to watch
--file      file inside the directory to watch
--command   the command to execute on changes
--arg       additional argument to pass to the command
```
## Help
```
hashmon --help
```
## Known Issues
### "No space left on device"
If the watched directory contains too many files the watch may fail with "No space left on device". 
On Linux increasing `fs.inotify.max_user_watches` may solve the issue.
